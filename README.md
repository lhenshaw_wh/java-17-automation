# Java-17-automation



## Overview

For a more comprehensive overview, see the relevant Ingestion confluence page: https://conf.willhillatlas.com/display/~cframpton/Java+17+automation+Script

This script updates the following files of a Java 8 or 11 app:
- README.md file
- Dockerfile 
- .gitlab-ci.yml file 
- pom.xml file 
- .java files containing common junit 4 statements

## Running the script locally
- Before running the script, ensure you have the relevant python libraries installed:
```
pip3 install ruamel.yaml
pip3 install pyyaml
```
### Using Multi-Gitter:
- In order to use the Multi-Gitter, a config.yaml file must be added to the project and configured to the application you are updating
- A base copy of the config.yaml file can be found on the above link
- the minimum changes that should be made are:
    - When using Multi-Gitter to update a Java application, This will automatically push changes to a new branch and create a merge request in the relevant gitlab repo
    - Enter your email in the author email field
    - add the name of the branch you want to create
    - add the path to the relevant gitlab repo, following the examples in the file
- more information about the multi-gitter can be found at: https://conf.willhillatlas.com/display/TRAD/Using+Multi-Gitter+with+example
- once the config file has been configured, run the following command to create a merge request of the changes made:
```
multi-gitter run "python3 filepath-to-python-script" --config=./config.yaml
```
- Note: The changes will not appear locally on your machine, you must checkout the created branch to view any changes locally

### Running with Python:
- To run the script without pushing to the gitlab repo, you can run the script without multi-gitter
- This will make the changes locally on the branch you run it from
- this allows for testing before pushing to a branch
```
python3 filepath-to-python-script
```
