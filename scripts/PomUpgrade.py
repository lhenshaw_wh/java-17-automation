import os
import xml.etree.ElementTree as ET
import JunitUpgrades


def _replace_parent(pom_contents, target_artifact_id, new_version, group_id):
    """
    updates the parent in the POM to match the required parent and version
    adds dependencies that become deprecated when updating the parent
    """
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    target_element = pom_contents.find('.//xmlns:parent[xmlns:artifactId="{}"]'.format(target_artifact_id), namespaces)
    if target_element is not None:
        version_element = target_element.find('xmlns:version', namespaces)
        if version_element is not None:
            version_element.text = new_version

    else:
        parent_element = pom_contents.find('.//xmlns:parent', namespaces)
        if parent_element is not None:
            parent_element.find('xmlns:groupId', namespaces).text = group_id
            parent_element.find('xmlns:artifactId', namespaces).text = target_artifact_id
            parent_element.find('xmlns:version', namespaces).text = new_version


def _replace_property_version(pom_contents, new_version, element_name):
    """
    replaces the version or adds an element to the properties section of the POM
    """
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    target_element = pom_contents.find('.//xmlns:properties', namespaces)
    if target_element is not None:
        version_element = target_element.find('xmlns:{}'.format(element_name), namespaces)
        if version_element is not None:
            version_element.text = new_version
        elif element_name not in ET.tostring(target_element, encoding='utf-8').decode('utf-8'):
            ET.SubElement(target_element, element_name).text = new_version
            _add_format_to_pom(pom_contents, target_element)


def delete_dependency(pom_contents, groupid, artifactid):
    """
    finds and removes dependencies in the POM if they are present
    """
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    dependencies_element_list = pom_contents.findall('.//xmlns:dependencies', namespaces)

    if dependencies_element_list is not None:
        for dep in dependencies_element_list:

            if dep is not None:
                existing_dependency = dep.find(
                    './/xmlns:dependency[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(groupid, artifactid),
                    namespaces)

            if existing_dependency is not None:
                # Dependency exists, remove it
                dep.remove(existing_dependency)



def update_or_add_dependency(pom_contents, groupid, artifactid, version_number, version_name):
    """
    finds and updates a specified dependencies version, or adds the dependency if not present
    """
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}
    is_updated = _update_existing_dependencies(pom_contents, groupid, artifactid, version_number, version_name)
    if not is_updated:
        dep_man_element = pom_contents.find('.//xmlns:dependencyManagement', namespaces)
        build_section = pom_contents.find('.//xmlns:build', namespaces)
        if build_section is not None:
            plugin_element = build_section.find('.//xmlns:plugins', namespaces)
        if plugin_element is not None:
            plugin_element_list = plugin_element.findall('.//xmlns:plugin', namespaces)

        dependencies_element_list = pom_contents.findall('.//xmlns:dependencies', namespaces)
        if dependencies_element_list is not None:
            for dep in dependencies_element_list:
                is_main_dependencies = True
                if dep_man_element is not None:
                    if dep in dep_man_element:
                        is_main_dependencies = False

                if plugin_element_list is not None:
                    for plugin in plugin_element_list:
                        if dep in plugin:
                            is_main_dependencies = False

                if dep is not None:
                    if is_main_dependencies or len(dependencies_element_list) == 1:
                        _replace_property_version(pom_contents, version_number, version_name)
                        _add_format_to_pom(pom_contents, dep)
                        # Dependency doesn't exist, create and add it
                        new_dependency = ET.SubElement(dep, 'dependency')
                        _add_format_to_pom(pom_contents, new_dependency)
                        ET.SubElement(new_dependency, 'groupId').text = groupid
                        _add_format_to_pom(pom_contents, new_dependency)
                        ET.SubElement(new_dependency, 'artifactId').text = artifactid
                        _add_format_to_pom(pom_contents, new_dependency)
                        ET.SubElement(new_dependency, 'version').text = "${" + version_name + "}"
                        _add_format_to_pom(pom_contents, new_dependency)
                        _add_format_to_pom(pom_contents, dep)


def _update_existing_dependencies(pom_contents, groupid, artifactid, version_number, version_name):
    """
    updates a specified existing dependencies version (does not add dependency if not present)
    """
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    dependencies_element_list = pom_contents.findall('.//xmlns:dependencies', namespaces)

    if dependencies_element_list is not None:
        for dep in dependencies_element_list:
            if dep is not None:
                existing_dependency = dep.find(
                    './/xmlns:dependency[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(groupid, artifactid),
                    namespaces)

            if existing_dependency is not None:
                _replace_property_version(pom_contents, version_number, version_name)
                if existing_dependency.find('xmlns:version', namespaces) is not None:
                    existing_dependency.find('xmlns:version', namespaces).text = "${" + version_name + "}"
                else:
                    ET.SubElement(existing_dependency, 'version').text = "${" + version_name + "}"
                    _add_format_to_pom(pom_contents, existing_dependency)
                return True
    return False


def _add_exclusion_if_not_exists(pom_contents, group_id, artifact_id, exclusion_group, exclusion_artifact):
    """
    adds a specified exclusion element to a specified dependency if exclusion is not present
    """
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    dependencies_element_list = pom_contents.findall('.//xmlns:dependencies', namespaces)

    if dependencies_element_list is not None:
        for dep in dependencies_element_list:

            if dep is not None:
                # Find the dependency element with the specified group_id and artifact_id
                dependency_element = dep.find(
                    './/xmlns:dependency[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(group_id, artifact_id),
                    namespaces)

                if dependency_element is not None:
                    # Check if the exclusion already exists
                    exclusion_element = dependency_element.find('.//xmlns:exclusions', namespaces)
                    if exclusion_element is not None:
                        exclusions_element = exclusion_element.find(
                            './/xmlns:exclusion[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(exclusion_group,
                                                                                                   exclusion_artifact),
                            namespaces)

                    if exclusion_element is None or exclusions_element is None:
                        # Exclusion doesn't exist, create and add it
                        exclusions = ET.SubElement(dependency_element, 'exclusions')
                        _add_format_to_pom(pom_contents, exclusions)
                        new_exclusion = ET.SubElement(exclusions, 'exclusion')
                        _add_format_to_pom(pom_contents, new_exclusion)
                        ET.SubElement(new_exclusion, 'groupId').text = exclusion_group
                        _add_format_to_pom(pom_contents, new_exclusion)
                        ET.SubElement(new_exclusion, 'artifactId').text = exclusion_artifact
                        _add_format_to_pom(pom_contents, new_exclusion)
                        _add_format_to_pom(pom_contents, exclusions)
                        _add_format_to_pom(pom_contents, dependency_element)


def _add_spring_boot_maven_plugin_exclusions_if_missing(pom_contents, group_id, artifact_id):
    """
    adds an execution element to a specified plugin if execution is not present
    """
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}
    build_section = pom_contents.find('.//xmlns:build', namespaces)
    # Find the plugins section
    if build_section is not None:
        plugins_section = build_section.find('.//xmlns:plugins', namespaces)

        if plugins_section is not None:
            # Check if the plugin already exists
            plugin_element = plugins_section.find(
                './/xmlns:plugin[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(group_id, artifact_id), namespaces)

            if plugin_element is not None:
                # Check if executions already exist
                executions_element = plugin_element.find('.//xmlns:executions', namespaces)

                if executions_element is None:
                    # Add executions block if it doesn't exist
                    executions_element = ET.SubElement(plugin_element, 'executions')
                    _add_format_to_pom(pom_contents, executions_element)
                    execution_element = ET.SubElement(executions_element, 'execution')
                    _add_format_to_pom(pom_contents, execution_element)
                    goals_element = ET.SubElement(execution_element, 'goals')
                    _add_format_to_pom(pom_contents, goals_element)
                    ET.SubElement(goals_element, 'goal').text = 'build-info'
                    _add_format_to_pom(pom_contents, goals_element)
                    configuration_element = ET.SubElement(execution_element, 'configuration')
                    _add_format_to_pom(pom_contents, configuration_element)
                    additional_properties = ET.SubElement(configuration_element, 'additionalProperties')
                    ET.SubElement(additional_properties, 'encoding.source').text = '${project.build.sourceEncoding}'
                    _add_format_to_pom(pom_contents, additional_properties)
                    ET.SubElement(additional_properties,
                                  'encoding.reporting').text = '${project.reporting.outputEncoding}'
                    _add_format_to_pom(pom_contents, additional_properties)
                    ET.SubElement(additional_properties, 'java.source').text = '${maven.compiler.source}'
                    _add_format_to_pom(pom_contents, additional_properties)
                    ET.SubElement(additional_properties, 'java.target').text = '${maven.compiler.target}'
                    _add_format_to_pom(pom_contents, additional_properties)
                    _add_format_to_pom(pom_contents, configuration_element)
                    _add_format_to_pom(pom_contents, execution_element)
                    _add_format_to_pom(pom_contents, executions_element)
                    _add_format_to_pom(pom_contents, plugin_element)


def _replace_plugin_config_element(pom_contents, group_id, artifact_id):
    """
    replaces the config element of the jgitflow plugin to prevent untracked files causing an error
    """
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}
    build_section = pom_contents.find('.//xmlns:build', namespaces)
    # Find the plugins section
    if build_section is not None:
        plugins_section = build_section.find('.//xmlns:plugins', namespaces)

        if plugins_section is not None:
            # Check if the plugin already exists
            plugin_element = plugins_section.find('.//xmlns:plugin[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(group_id, artifact_id), namespaces)

            if plugin_element is not None:
                if plugin_element.find('xmlns:version', namespaces) is None:
                    _add_format_to_pom(pom_contents, plugin_element)
                    ET.SubElement(plugin_element, 'version').text = '${jgitflow-maven-plugin.version}'
                    _add_format_to_pom(pom_contents, plugin_element)
                else:
                    plugin_element.find('xmlns:version', namespaces).text = '${jgitflow-maven-plugin.version}'
                _replace_property_version(pom_contents, '1.0-m5.1', 'jgitflow-maven-plugin.version')

                # Check if configuration already exist
                configuration_element = plugin_element.find('.//xmlns:configuration', namespaces)

                if configuration_element is not None:
                    plugin_element.remove(configuration_element)
                new_configuration_element = ET.SubElement(plugin_element, 'configuration')
                _add_format_to_pom(pom_contents, new_configuration_element)
                ET.SubElement(new_configuration_element, 'allowUntracked').text = "true"
                _add_format_to_pom(pom_contents, new_configuration_element)
                ET.SubElement(new_configuration_element, 'allowSnapshots').text = "true"
                _add_format_to_pom(pom_contents, new_configuration_element)
                ET.SubElement(new_configuration_element, 'keepBranch').text = "false"
                _add_format_to_pom(pom_contents, new_configuration_element)
                ET.SubElement(new_configuration_element, 'username').text = "${git.user}"
                _add_format_to_pom(pom_contents, new_configuration_element)
                ET.SubElement(new_configuration_element, 'password').text = "${git.password}"
                _add_format_to_pom(pom_contents, new_configuration_element)
                _add_format_to_pom(pom_contents, plugin_element)


def _update_spring_boot_maven_plugin(pom_contents):
    """
    updates the spring_boot_maven_plugin by calling the relevant methods
    """
    _replace_property_version(pom_contents, '17', 'maven.compiler.source')
    _replace_property_version(pom_contents, '17', 'maven.compiler.target')
    _add_spring_boot_maven_plugin_exclusions_if_missing(pom_contents, 'org.springframework.boot', 'spring-boot-maven-plugin')


def return_java_file_list(extension):
    """
    finds and returns a list of files that end in a particular extension
    """
    filename_list = []
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(extension)]:
            filename_list.append(os.path.join(dirpath, filename))

    return filename_list


def _search_for_imports(filename_list, target_import):
    """
    searches a list of java files for a specific import
    """
    for filename in filename_list:
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                content = file.read()
                if target_import in content:
                    return True
        except Exception as e:
            print('Error reading "{}", exception "{}"'.format(filename, e))
    return False


def _add_jaxb_dependencies(pom_contents):
    """
    adds a number of dependencies to the POM if certain imports are present in the project
    """
    file_names = return_java_file_list(".java")
    if _search_for_imports(file_names, "import javax.xml") or _search_for_imports(file_names, "import com.fasterxml.jackson"):
        update_or_add_dependency(pom_contents, "javax.xml.bind", "jaxb-api", "2.3.1", "javax.version")
        update_or_add_dependency(pom_contents, "com.sun.xml.bind", "jaxb-core", "3.0.2", "jaxb.version")
        update_or_add_dependency(pom_contents, "com.sun.xml.bind", "jaxb-impl", "3.0.2", "jaxb.version")
        update_or_add_dependency(pom_contents, "org.glassfish.jaxb", "jaxb-runtime", "2.3.1", "javax.version")
        update_or_add_dependency(pom_contents, "jakarta.xml.bind", "jakarta.xml.bind-api", "3.0.1", "jakarta.version")
        update_or_add_dependency(pom_contents, "javax.activation", "activation", "1.1.1", "activation.version")
        update_or_add_dependency(pom_contents, "com.fasterxml.jackson.datatype", "jackson-datatype-joda", "2.12.3", "jackson.version")


def update_pom(file_path):
    tree = ET.parse(file_path)
    pom_contents = tree.getroot()

    _replace_parent(pom_contents, 'spring-boot-starter-parent', '2.7.7', 'org.springframework.boot')
    _add_exclusion_if_not_exists(pom_contents, 'org.springframework.boot', 'spring-boot-starter-web', 'org.springframework.boot', 'spring-boot-starter-logging')
    _update_existing_dependencies(pom_contents, 'de.codecentric', 'spring-boot-admin-starter-client', '2.7.7', 'spring-boot-admin-starter-client.version')
    update_or_add_dependency(pom_contents, 'org.mockito', 'mockito-core', '5.8.0', 'mockito.version')
    _replace_property_version(pom_contents, '17', 'java.version')
    _update_existing_dependencies(pom_contents, 'com.williamhill.trading.platform', 'logging-commons', '0.21', 'logging-commons.version')
    _update_existing_dependencies(pom_contents, 'org.projectlombok', 'lombok', '1.18.30', 'lombok.version')
    _add_jaxb_dependencies(pom_contents)
    _update_spring_boot_maven_plugin(pom_contents)
    _replace_plugin_config_element(pom_contents, 'external.atlassian.jgitflow', 'jgitflow-maven-plugin')

    JunitUpgrades.update_junit(pom_contents, return_java_file_list(".java"))

    modified_xml = _add_format_to_pom(pom_contents, pom_contents)

    with open(file_path, "w") as file:
        file.write(modified_xml)


def _add_format_to_pom(pom_contents, element):
    """
    method to keep pom readable and ensure the format is correct
    """
    if pom_contents == element:
        modified_xml = ET.tostring(pom_contents, encoding='utf-8').decode('utf-8')
        modified_xml = '<?xml version="1.0" encoding="UTF-8"?>\n' + modified_xml + '\n'
        modified_xml = modified_xml.replace('ns0:', '')
        modified_xml = modified_xml.replace(':ns0', '')
        modified_xml = modified_xml.replace('<!---->', '\n  ')
        return modified_xml
    else:
        element.append(ET.Comment(""))
