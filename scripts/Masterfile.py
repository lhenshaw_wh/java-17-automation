import ReadMeUpgrade
import DockerfileUpgrade
import Gitlab_ciUpgrade
import PomUpgrade
import os


def _return_filename(file_name):
    """
    takes a filename as a parameter and
    walks through the project directory to return the specified file
    """
    dlist = []
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(file_name)]:
            dlist.append(os.path.join(dirpath, filename))

    for file in dlist:
        filename: str = file
        print(filename)
        return filename


if __name__ == "__main__":
    ReadMeUpgrade.update_read_me(_return_filename('README.md'))
    DockerfileUpgrade.update_dockerfile(_return_filename("Dockerfile"))
    Gitlab_ciUpgrade.update_gitlab_ci_file(_return_filename("gitlab-ci.yml"))
    PomUpgrade.update_pom(_return_filename("pom.xml"))
