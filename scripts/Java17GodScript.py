import os
import yaml
import xml.etree.ElementTree as ET
import re


"""
takes a filename as a parameter and
walks through the project directory to return the specified file
"""
def return_filename(file_name):
    dlist = []
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(file_name)]:
            dlist.append(os.path.join(dirpath, filename))

    for file in dlist:
        filename: str = file
        print(filename)
        return filename

"""
updates the project README.md file to show it has been updated to Java 17
"""
def add_line_to_readme(file_path, line_to_add):
    with open(file_path, 'r') as file:
        content = file.read()

    if content.find(line_to_add) == -1:
        with open(file_path, 'w') as file:
            file.write(f"{line_to_add}\n{content}")

"""
finds the README.md file and updates it by calling the relevant methods
"""
def update_read_me(file_path):
    line_to_add = 'Automated Upgrade to Java 17'
    add_line_to_readme(file_path, line_to_add)


"""
updates the Dockerfile to use the java 17 image
"""
def removeDockerRegistry(dockerfile):
    # replace docker-registery with ecr
    new_image = '663553139883.dkr.ecr.eu-west-1.amazonaws.com/platform/docker-images/trading-new-relic:ol7-java17'
    paths_to_remove = [
        'nexus-aws.dtc.prod.williamhill.plc/trading/trading-new-relic:ol7',
        '663553139883.dkr.ecr.eu-west-1.amazonaws.com/platform/docker-images/trading-new-relic:ol7',
        'docker-registry.prod.williamhill.plc/trading/whc-trading-java-newrelic:java8',
    ]
    # Load
    handle = open(dockerfile, "r")
    content = handle.read()

    for path in paths_to_remove:
        if path in content and content.find(new_image) == -1:
            content = content.replace(path, new_image)

    handle.close()

    # Not the most resilient as if pod gets bapped then we will get an empty Docker file

    # OVERWRITE
    handle = open(dockerfile, "w")
    handle.write(content)
    handle.close()


"""
finds the Dockerfile and updates it by calling the relevant methods
"""
def update_dockerfile(file_path):
    if file_path is not None:
        removeDockerRegistry(file_path)


"""
restructures the gitlab-ci.yaml file
"""
def process_yaml(gitlab_file):
    with open(gitlab_file, 'r') as file:
        data = yaml.safe_load(file)

    data['include'] = [{'project': 'williamhillplc/trading/platform/gitlab-templates',
                        'file': '.gitlab-ci-maven-java-auto-template.yml'}]

    # Keep 'tech_STACK' and add 'ENABLE_SNAPSHOT'
    data['variables']['ENABLE_SNAPSHOT'] = 'true'

    # Keep only the 'include' part for project and file tags
    keys_to_delete = [
        "FF_GITLAB_REGISTRY_HELPER_IMAGE",
        "KUBERNETES_MEMORY_REQUEST",
        "KUBERNETES_MEMORY_LIMIT",
        "GIT_STRATEGY",
        "MAVEN_OPTS",
        "ECR_ENABLED",
        "FILE_NAME",
        "IS_LATEST",
        "PREFIX",
    ]

    if "variables" in data:
        for key in keys_to_delete:
            data["variables"].pop(key, None)

    # Remove unnecessary sections
    sections_to_remove = [
        'build',
        'cache',
        'docker-aws-release-push',
        'docker-aws-snapshot-push',
        'docker-whc-release-push',
        'docker-whc-snapshot-push',
        'stages',
        'tag-project',
        'integration-test',
        'trigger-deploy',
        'sonarqube-scan',
        'dev:sonarqube:scan',
        'before_script',
        'dev:docker:snapshot:push',
        'prod:docker:release:push',
        'dev:tag:project',
    ]
    for section in sections_to_remove:
        if section in data:
            del data[section]

    with open(gitlab_file, 'w') as file_contents:
        yaml.safe_dump(data, file_contents)


"""
finds the gitlab-ci.yaml file and updates it by calling the relevant methods
"""
def update_gitlab_ci_file(file_path):
    process_yaml(file_path)


"""
updates the parent in the POM to match the required parent and version
adds dependencies that become deprecated when updating the parent
"""
def replace_parent(root, target_artifact_id, new_version, group_id):
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    target_element = root.find('.//xmlns:parent[xmlns:artifactId="{}"]'.format(target_artifact_id), namespaces)
    if target_element is not None:
        version_element = target_element.find('xmlns:version', namespaces)
        if version_element is not None:
            version_element.text = new_version

    else:
        parent_element = root.find('.//xmlns:parent', namespaces)
        if parent_element is not None:
            parent_element.find('xmlns:groupId', namespaces).text = group_id
            parent_element.find('xmlns:artifactId', namespaces).text = target_artifact_id
            parent_element.find('xmlns:version', namespaces).text = new_version


"""
replaces the version or adds an element to the properties section of the POM
"""
def replace_property_version(root, new_version, element_name):
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    target_element = root.find('.//xmlns:properties', namespaces)
    if target_element is not None:
        version_element = target_element.find('xmlns:{}'.format(element_name), namespaces)
        if version_element is not None:
            version_element.text = new_version
        elif element_name not in ET.tostring(target_element, encoding='utf-8').decode('utf-8'):
            ET.SubElement(target_element, element_name).text = new_version
            add_format_to_pom(root, target_element)


"""
finds and removes dependencies in the POM if they are present
"""
def delete_dependency(root, groupid, artifactid):
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    dependencies_element_list = root.findall('.//xmlns:dependencies', namespaces)

    if dependencies_element_list is not None:
        for dep in dependencies_element_list:

            if dep is not None:
                existing_dependency = dep.find(
                    './/xmlns:dependency[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(groupid, artifactid),
                    namespaces)

            if existing_dependency is not None:
                # Dependency exists, remove it
                dep.remove(existing_dependency)


"""
finds and updates a specified dependencies version, or adds the dependency if not present
"""
def update_or_add_dependency(root, groupid, artifactid, version_number, version_name):
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}
    is_updated = update_existing_dependencies(root, groupid, artifactid, version_number, version_name)
    if not is_updated:
        dep_man_element = root.find('.//xmlns:dependencyManagement', namespaces)
        build_section = root.find('.//xmlns:build', namespaces)
        if build_section is not None:
            plugin_element = build_section.find('.//xmlns:plugins', namespaces)
        if plugin_element is not None:
            plugin_element_list = plugin_element.findall('.//xmlns:plugin', namespaces)

        dependencies_element_list = root.findall('.//xmlns:dependencies', namespaces)
        if dependencies_element_list is not None:
            for dep in dependencies_element_list:
                is_main_dependencies = True
                if dep_man_element is not None:
                    if dep in dep_man_element:
                        is_main_dependencies = False

                if plugin_element_list is not None:
                    for plugin in plugin_element_list:
                        if dep in plugin:
                            is_main_dependencies = False

                if dep is not None:
                    if is_main_dependencies or len(dependencies_element_list) == 1:
                        replace_property_version(root, version_number, version_name)
                        add_format_to_pom(root, dep)
                        # Dependency doesn't exist, create and add it
                        new_dependency = ET.SubElement(dep, 'dependency')
                        add_format_to_pom(root, new_dependency)
                        ET.SubElement(new_dependency, 'groupId').text = groupid
                        add_format_to_pom(root, new_dependency)
                        ET.SubElement(new_dependency, 'artifactId').text = artifactid
                        add_format_to_pom(root, new_dependency)
                        ET.SubElement(new_dependency, 'version').text = "${" + version_name + "}"
                        add_format_to_pom(root, new_dependency)
                        add_format_to_pom(root, dep)


"""
updates a specified existing dependencies version (does not add dependency if not present)
"""
def update_existing_dependencies(root, groupid, artifactid, version_number, version_name):
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    dependencies_element_list = root.findall('.//xmlns:dependencies', namespaces)

    if dependencies_element_list is not None:
        for dep in dependencies_element_list:
            if dep is not None:
                existing_dependency = dep.find(
                    './/xmlns:dependency[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(groupid, artifactid),
                    namespaces)

            if existing_dependency is not None:
                replace_property_version(root, version_number, version_name)
                if existing_dependency.find('xmlns:version', namespaces) is not None:
                    existing_dependency.find('xmlns:version', namespaces).text = "${" + version_name + "}"
                else:
                    ET.SubElement(existing_dependency, 'version').text = "${" + version_name + "}"
                    add_format_to_pom(root, existing_dependency)
                return True
    return False


"""
adds a specified exclusion element to a specified dependency if exclusion is not present
"""
def add_exclusion_if_not_exists(root, group_id, artifact_id, exclusion_group, exclusion_artifact):
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}

    dependencies_element_list = root.findall('.//xmlns:dependencies', namespaces)

    if dependencies_element_list is not None:
        for dep in dependencies_element_list:

            if dep is not None:
                # Find the dependency element with the specified group_id and artifact_id
                dependency_element = dep.find(
                    './/xmlns:dependency[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(group_id, artifact_id),
                    namespaces)

                if dependency_element is not None:
                    # Check if the exclusion already exists
                    exclusion_element = dependency_element.find('.//xmlns:exclusions', namespaces)
                    if exclusion_element is not None:
                        exclusions_element = exclusion_element.find(
                            './/xmlns:exclusion[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(exclusion_group,
                                                                                                   exclusion_artifact),
                            namespaces)

                    if exclusion_element is None or exclusions_element is None:
                        # Exclusion doesn't exist, create and add it
                        exclusions = ET.SubElement(dependency_element, 'exclusions')
                        add_format_to_pom(root, exclusions)
                        new_exclusion = ET.SubElement(exclusions, 'exclusion')
                        add_format_to_pom(root, new_exclusion)
                        ET.SubElement(new_exclusion, 'groupId').text = exclusion_group
                        add_format_to_pom(root, new_exclusion)
                        ET.SubElement(new_exclusion, 'artifactId').text = exclusion_artifact
                        add_format_to_pom(root, new_exclusion)
                        add_format_to_pom(root, exclusions)
                        add_format_to_pom(root, dependency_element)


"""
adds an execution element to a specified plugin if execution is not present
"""
def add_spring_boot_maven_plugin_exclusions_if_missing(root, group_id, artifact_id):
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}
    build_section = root.find('.//xmlns:build', namespaces)
    # Find the plugins section
    if build_section is not None:
        plugins_section = build_section.find('.//xmlns:plugins', namespaces)

        if plugins_section is not None:
            # Check if the plugin already exists
            plugin_element = plugins_section.find(
                './/xmlns:plugin[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(group_id, artifact_id), namespaces)

            if plugin_element is not None:
                # Check if executions already exist
                executions_element = plugin_element.find('.//xmlns:executions', namespaces)

                if executions_element is None:
                    # Add executions block if it doesn't exist
                    executions_element = ET.SubElement(plugin_element, 'executions')
                    add_format_to_pom(root, executions_element)
                    execution_element = ET.SubElement(executions_element, 'execution')
                    add_format_to_pom(root, execution_element)
                    goals_element = ET.SubElement(execution_element, 'goals')
                    add_format_to_pom(root, goals_element)
                    ET.SubElement(goals_element, 'goal').text = 'build-info'
                    add_format_to_pom(root, goals_element)
                    configuration_element = ET.SubElement(execution_element, 'configuration')
                    add_format_to_pom(root, configuration_element)
                    additional_properties = ET.SubElement(configuration_element, 'additionalProperties')
                    ET.SubElement(additional_properties, 'encoding.source').text = '${project.build.sourceEncoding}'
                    add_format_to_pom(root, additional_properties)
                    ET.SubElement(additional_properties,
                                  'encoding.reporting').text = '${project.reporting.outputEncoding}'
                    add_format_to_pom(root, additional_properties)
                    ET.SubElement(additional_properties, 'java.source').text = '${maven.compiler.source}'
                    add_format_to_pom(root, additional_properties)
                    ET.SubElement(additional_properties, 'java.target').text = '${maven.compiler.target}'
                    add_format_to_pom(root, additional_properties)
                    add_format_to_pom(root, configuration_element)
                    add_format_to_pom(root, execution_element)
                    add_format_to_pom(root, executions_element)
                    add_format_to_pom(root, plugin_element)


"""
replaces the config element of the jgitflow plugin to prevent untracked files causing an error
"""
def replace_plugin_config_element(root, group_id, artifact_id):
    namespaces = {"xmlns": "http://maven.apache.org/POM/4.0.0"}
    build_section = root.find('.//xmlns:build', namespaces)
    # Find the plugins section
    if build_section is not None:
        plugins_section = build_section.find('.//xmlns:plugins', namespaces)

        if plugins_section is not None:
            # Check if the plugin already exists
            plugin_element = plugins_section.find(
                './/xmlns:plugin[xmlns:groupId="{}"][xmlns:artifactId="{}"]'.format(group_id, artifact_id), namespaces)
            if plugin_element.find('xmlns:version', namespaces) is None:
                add_format_to_pom(root, plugin_element)
                ET.SubElement(plugin_element, 'version').text = '${jgitflow-maven-plugin.version}'
                add_format_to_pom(root, plugin_element)
                replace_property_version(root, '1.0-m5.1', 'jgitflow-maven-plugin.version')

            if plugin_element is not None:
                # Check if executions already exist
                configuration_element = plugin_element.find('.//xmlns:configuration', namespaces)

                if configuration_element is not None:
                    plugin_element.remove(configuration_element)
                new_configuration_element = ET.SubElement(plugin_element, 'configuration')
                add_format_to_pom(root, new_configuration_element)
                ET.SubElement(new_configuration_element, 'allowUntracked').text = "true"
                add_format_to_pom(root, new_configuration_element)
                ET.SubElement(new_configuration_element, 'allowSnapshots').text = "true"
                add_format_to_pom(root, new_configuration_element)
                ET.SubElement(new_configuration_element, 'keepBranch').text = "false"
                add_format_to_pom(root, new_configuration_element)
                ET.SubElement(new_configuration_element, 'username').text = "${git.user}"
                add_format_to_pom(root, new_configuration_element)
                ET.SubElement(new_configuration_element, 'password').text = "${git.password}"
                add_format_to_pom(root, new_configuration_element)
                add_format_to_pom(root, plugin_element)


"""
updates the spring_boot_maven_plugin by calling the relevant methods
"""
def update_spring_boot_maven_plugin(root):
    replace_property_version(root, '17', 'maven.compiler.source')
    replace_property_version(root, '17', 'maven.compiler.target')
    add_spring_boot_maven_plugin_exclusions_if_missing(root, 'org.springframework.boot', 'spring-boot-maven-plugin')


"""
finds and returns a list of files that end in a particular extension
"""
def return_java_file_list(extension):
    filename_list = []
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(extension)]:
            filename_list.append(os.path.join(dirpath, filename))

    return filename_list


"""
searches a list of java files for a specific import
"""
def search_for_imports(filename_list, target_import):
    for filename in filename_list:
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                content = file.read()
                if target_import in content:
                    return True
        except Exception as e:
            print('Error reading "{}", exception "{}"'.format(filename, e))
    return False


"""
adds a number of dependencies to the POM if certain imports are present in the project
"""
def add_jaxb_dependencies(root):
    file_names = return_java_file_list(".java")
    if search_for_imports(file_names, "import javax.xml"):
        update_or_add_dependency(root, "javax.xml.bind", "jaxb-api", "2.3.1", "javax.version")
        update_or_add_dependency(root, "com.sun.xml.bind", "jaxb-core", "3.0.2", "jaxb.version")
        update_or_add_dependency(root, "com.sun.xml.bind", "jaxb-impl", "3.0.2", "jaxb.version")
        update_or_add_dependency(root, "org.glassfish.jaxb", "jaxb-runtime", "2.3.1", "javax.version")
        update_or_add_dependency(root, "jakarta.xml.bind", "jakarta.xml.bind-api", "3.0.1", "jakarta.version")
        update_or_add_dependency(root, "javax.activation", "activation", "1.1.1", "activation.version")
        update_or_add_dependency(root, "com.fasterxml.jackson.datatype", "jackson-datatype-joda", "2.12.3", "jackson.version")


def update_pom(file_path):
    tree = ET.parse(file_path)
    root = tree.getroot()

    replace_parent(root, 'spring-boot-starter-parent', '2.7.7', 'org.springframework.boot')
    add_exclusion_if_not_exists(root, 'org.springframework.boot', 'spring-boot-starter-web', 'org.springframework.boot','spring-boot-starter-logging')
    update_existing_dependencies(root, 'de.codecentric', 'spring-boot-admin-starter-client', '2.7.7','spring-boot-admin-starter-client.version')
    update_or_add_dependency(root, 'org.mockito', 'mockito-core', '5.8.0', 'mockito.version')
    replace_property_version(root, '17', 'java.version')
    update_existing_dependencies(root, 'com.williamhill.trading.platform', 'logging-commons', '0.21','logging-commons.version')
    update_existing_dependencies(root, 'org.projectlombok', 'lombok', '1.18.22', 'lombok.version')
    add_jaxb_dependencies(root)
    update_spring_boot_maven_plugin(root)
    replace_plugin_config_element(root, 'external.atlassian.jgitflow', 'jgitflow-maven-plugin')

    update_junit(root, return_java_file_list(".java"))

    modified_xml = add_format_to_pom(root, root)

    with open(file_path, "w") as file:
        file.write(modified_xml)

"""
method to keep pom readable and ensure the format is correct
"""
def add_format_to_pom(root, element):
    if root == element:
        modified_xml = ET.tostring(root, encoding='utf-8').decode('utf-8')
        modified_xml = '<?xml version="1.0" encoding="UTF-8"?>\n' + modified_xml + '\n'
        modified_xml = modified_xml.replace('ns0:', '')
        modified_xml = modified_xml.replace(':ns0', '')
        modified_xml = modified_xml.replace('<!---->', '\n  ')
        return modified_xml
    else:
        element.append(ET.Comment(""))


"""
finds and returns a list of files that end in a particular extension
"""
def return_java_file_list(extension):
    filename_list = []
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in [f for f in filenames if f.endswith(extension)]:
            filename_list.append(os.path.join(dirpath, filename))

    return filename_list


"""
searches a given file for a specific import and replaces it with a new given import
"""
def update_junit_imports(filenames, old_import, new_import):
    for filename in filenames:
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                lines = file.readlines()

            with open(filename, 'w', encoding='utf-8') as file:
                new_import_added = False

                for line in lines:
                    if new_import in line:
                        # The new import is already present
                        new_import_added = True
                        file.write(line)
                    elif old_import in line and not new_import_added:
                        file.write(f"{new_import}\n")
                        new_import_added = True
                    elif old_import in line and new_import_added:
                        file.write("")
                    else:
                        file.write(line)

        except Exception as e:
            print(f"Error updating imports in {filename}: {e}")



"""
searches a file for a specific statement and replaces it with a new one
"""
def replace_junit_statements(filenames, old_line, new_line):
    for filename in filenames:
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                lines = file.readlines()

            with open(filename, 'w', encoding='utf-8') as file:
                for line in lines:
                    if "import" not in line and new_line not in line:
                        updated_line = line.replace(old_line, new_line)
                        file.write(updated_line)
                    else:
                        file.write(line)

        except Exception as e:
            print(f"Error replacing assert statements in {filename}: {e}")



"""
searches any .java extension files for tests with exeptions next to the @Test annotation and updates the test as these are no longer supported
layout of expected junit 4 test:
    @Test(expected = exception.class)
    public void testName() throws exception {
        test body;
    }
layout of updated junit 5 test:
    @Test
    public void testName() {
        assertThrows(exception.class, () -> {
            test body;
        });
    }
where exception.class can be replaced by any exception type e.g. IllegalArgumentException.class
"""
def junit_remove_annotated_test_exception_statements(filenames):
    pattern = re.compile(r'@Test\(expected = (.+?)\)\s+public void (\w+)\(\)(?: throws .+?)? \{(.+?)\}', re.DOTALL)

    for filename in filenames:
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                file_content = file.read()

            updated_content = pattern.sub(
                r'@Test\n    public void \2() {\n    assertThrows(\1, () -> {\n        \3\n    });\n}',  # Backreferences insert each section captured by the pattern
                file_content
            )

            with open(filename, 'w', encoding='utf-8') as file:
                file.write(updated_content)

        except Exception as e:
            print(f"Error replacing test statements in {filename}: {e}")


"""
updates junit dependencies to junit-jupiter in the POM, then searches the project for common junit code and replaces it with the equivalent junit-jupiter code
"""
def update_junit(root, filenames):
    delete_dependency(root, 'junit', 'junit')
    update_or_add_dependency(root, "org.junit.jupiter", "junit-jupiter", "5.9.3", "junit-jupiter.version")
    update_or_add_dependency(root, "org.junit.jupiter", "junit-jupiter-api", "5.9.3", "junit-jupiter.version")
    update_or_add_dependency(root, "org.junit.jupiter", "junit-jupiter-engine", "5.9.3", "junit-jupiter.version")
    junit_remove_annotated_test_exception_statements(filenames)
    update_junit_imports(filenames, "import static org.junit.Assert.assertThat;", "import static org.hamcrest.MatcherAssert.assertThat;")
    update_junit_imports(filenames, "import static org.mockito.Mockito.verifyZeroInteractions;", "import static org.mockito.Mockito.verifyNoInteractions;")
    replace_junit_statements(filenames, "verifyZeroInteractions", "verifyNoInteractions")
    update_junit_imports(filenames, "import org.junit.Test;", "import org.junit.jupiter.api.Test;")
    update_junit_imports(filenames, "import static junit.framework.TestCase.assertTrue;", "import static org.junit.jupiter.api.Assertions.assertTrue;")
    update_junit_imports(filenames, "import org.junit.runner.RunWith;", "import org.junit.jupiter.api.extension.ExtendWith;")
    replace_junit_statements(filenames, "@RunWith(SpringRunner.class)", "@ExtendWith(SpringExtension.class)")
    replace_junit_statements(filenames, "@RunWith(SpringJUnit4ClassRunner.class)", "@ExtendWith(SpringExtension.class)")
    replace_junit_statements(filenames, "@RunWith(MockitoJUnitRunner.class)", "@ExtendWith(MockitoExtension.class)")
    update_junit_imports(filenames, "import org.springframework.test.context.junit4.SpringRunner;", "import org.springframework.test.context.junit.jupiter.SpringExtension;")
    update_junit_imports(filenames, "import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;", "import org.springframework.test.context.junit.jupiter.SpringExtension;")
    update_junit_imports(filenames, "import static org.junit.Assert", "import static org.junit.jupiter.api.Assertions.*;")
    update_junit_imports(filenames, "import org.junit.*;", "import org.junit.jupiter.api.*;")
    update_junit_imports(filenames, "import org.mockito.junit.MockitoJUnitRunner;", "import org.mockito.junit.jupiter.MockitoExtension;")
    update_junit_imports(filenames, "import org.junit.Before;", "import org.junit.jupiter.api.BeforeEach;")
    update_junit_imports(filenames, "import org.junit.After;", "import org.junit.jupiter.api.AfterEach;")
    replace_junit_statements(filenames, "@AfterClass", "@AfterAll")
    replace_junit_statements(filenames, "@After\n", "@AfterEach\n")
    update_junit_imports(filenames, "import org.junit.Assert;", "import org.junit.jupiter.api.Assertions;")
    update_junit_imports(filenames, "import org.junit.Assert.", "import org.junit.jupiter.api.Assertions.*")
    update_junit_imports(filenames, "import org.junit.BeforeClass;", "import org.junit.jupiter.api.BeforeAll;")
    replace_junit_statements(filenames, "@BeforeClass", "@BeforeAll")
    update_junit_imports(filenames, "import org.junit.AfterClass;", "import org.junit.jupiter.api.AfterAll;")
    update_junit_imports(filenames, "import org.junit.Ignore;", "import org.junit.jupiter.api.Disabled;")
    replace_junit_statements(filenames, " Assert.assert", " Assertions.assert")
    replace_junit_statements(filenames, "@Before\n", "@BeforeEach\n")
    replace_junit_statements(filenames, "@Ignore", "@Disabled")

if __name__ == "__main__":
    update_read_me(return_filename('README.md'))
    update_dockerfile(return_filename("Dockerfile"))
    update_gitlab_ci_file(return_filename("gitlab-ci.yml"))
    update_pom(return_filename("pom.xml"))
