import yaml


def process_yaml(gitlab_file):
    """
    restructures the gitlab-ci.yaml file
    """
    with open(gitlab_file, 'r') as file:
        data = yaml.safe_load(file)

    tech_stack = data.get('variables', {}).get('TECH_STACK', None)

    data = {'include': [{'project': 'williamhillplc/trading/platform/gitlab-templates',
                         'file': '.gitlab-ci-maven-java-auto-template.yml'}],
            'variables': {}}

    data['variables']['ENABLE_SNAPSHOT'] = 'true'
    data['variables']['TECH_STACK'] = tech_stack

    with open(gitlab_file, 'w') as file_contents:
        yaml.safe_dump(data, file_contents)


def update_gitlab_ci_file(file_path):
    """
    finds the gitlab-ci.yaml file and updates it by calling the relevant methods
    """
    process_yaml(file_path)
