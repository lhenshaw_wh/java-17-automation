#!/bin/bash

echo "Installing Requirements"

python3 -m ensurepip --default-pip
python3 -m pip install -r /Users/lhenshaw/IdeaProjects/wh_lhenshaw/Java_17_Automation_Test/requirements.txt

yum install -y sudo

curl -s https://raw.githubusercontent.com/lindell/multi-gitter/master/install.sh | sh

echo "Running Java17GodScript.py multi-gitter"

multi-gitter run "python3 /Users/lhenshaw/IdeaProjects/wh_lhenshaw/Java_17_Automation_Test/scripts/Java17GodScript.py" --config=/Users/lhenshaw/IdeaProjects/wh_lhenshaw/Java_17_Automation_Test/config.yaml

echo "Finished"
