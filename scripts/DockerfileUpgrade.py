def _remove_docker_registry(dockerfile):
    """
    updates the Dockerfile to use the java 17 image
    """
    # replace docker-registery with ecr
    new_image = '663553139883.dkr.ecr.eu-west-1.amazonaws.com/platform/docker-images/trading-new-relic:ol7-java17'
    paths_to_remove = [
        'nexus-aws.dtc.prod.williamhill.plc/trading/trading-new-relic:ol7',
        'docker-registry.prod.williamhill.plc/trading/trading-new-relic:ol7',
        'docker-registry.prod.williamhill.plc/trading/whc-trading-java-newrelic:java8',
        '663553139883.dkr.ecr.eu-west-1.amazonaws.com/platform/docker-images/trading-new-relic:ol7-java11',
        '663553139883.dkr.ecr.eu-west-1.amazonaws.com/platform/docker-images/trading-new-relic:ol7',
    ]
    # Load
    handle = open(dockerfile, "r")
    content = handle.read()

    for path in paths_to_remove:
        if path in content and content.find(new_image) == -1:
            content = content.replace(path, new_image)

    handle.close()

    # Not the most resilient as if pod gets bapped then we will get an empty Docker file

    # OVERWRITE
    handle = open(dockerfile, "w")
    handle.write(content)
    handle.close()


def update_dockerfile(file_path):
    """
    finds the Dockerfile and updates it by calling the relevant methods
    """
    if file_path is not None:
        _remove_docker_registry(file_path)