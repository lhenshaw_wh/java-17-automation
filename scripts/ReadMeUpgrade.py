def _add_line_to_readme(file_path, line_to_add):
    """
    updates the project README.md file to show it has been updated to Java 17
    """
    with open(file_path, 'r') as file:
        content = file.read()

    if content.find(line_to_add) == -1:
        with open(file_path, 'w') as file:
            file.write(f"{line_to_add}\n{content}")


def update_read_me(file_path):
    """
    finds the README.md file and updates it by calling the relevant methods
    """
    line_to_add = 'Automated Upgrade to Java 17'
    _add_line_to_readme(file_path, line_to_add)
