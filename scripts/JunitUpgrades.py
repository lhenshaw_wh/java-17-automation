import re
import PomUpgrade


def _update_junit_imports(filenames, old_import, new_import):
    """
    searches a given file for a specific import and replaces it with a new given import
    """
    for filename in filenames:
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                lines = file.readlines()

            with open(filename, 'w', encoding='utf-8') as file:
                new_import_added = False

                for line in lines:
                    if new_import in line:
                        # The new import is already present
                        new_import_added = True
                        file.write(line)
                    elif old_import in line and not new_import_added:
                        file.write(f"{new_import}\n")
                        new_import_added = True
                    elif old_import in line and new_import_added:
                        file.write("")
                    else:
                        file.write(line)

        except Exception as e:
            print(f"Error updating imports in {filename}: {e}")


def _replace_junit_statements(filenames, old_line, new_line):
    """
    searches a file for a specific statement and replaces it with a new one
    """
    for filename in filenames:
        try:
            with open(filename, 'r', encoding='utf-8') as file:
                lines = file.readlines()

            with open(filename, 'w', encoding='utf-8') as file:
                for line in lines:
                    if "import" not in line and new_line not in line:
                        updated_line = line.replace(old_line, new_line)
                        file.write(updated_line)
                    else:
                        file.write(line)

        except Exception as e:
            print(f"Error replacing assert statements in {filename}: {e}")


def _junit_remove_annotated_test_exception_statements(filenames):
    """
    searches any .java extension files for tests with exceptions next to the @Test annotation and updates the test as these are no longer supported
    layout of expected junit 4 test:
    @Test(expected = exception.class)
    public void testName() throws exception {
        test body;
    }
    layout of updated junit 5 test:
    @Test
    public void testName() {
        assertThrows(exception.class, () -> {
            test body;
        });
    }
    where exception.class can be replaced by any exception type e.g. IllegalArgumentException.class
    """
    pattern_list = [
        re.compile(r'@Test\(expected = (.+?)\)\s+public void (\w+)\(\)(?: throws .+?)?\s*\{(.+?)\}', re.DOTALL),
        re.compile(r'@Test\(expected = (.+?)\)\s+public void (\w+)\(\)\s*\{(.+?)\}', re.DOTALL)
    ]

    for pattern in pattern_list:
        for filename in filenames:
            try:
                with open(filename, 'r', encoding='utf-8') as file:
                    file_content = file.read()

                updated_content = pattern.sub(
                    r'@Test\n    public void \2() {\n    assertThrows(\1, () -> {\n        \3\n    });\n}',  # Backreferences insert each section captured by the pattern
                    file_content
                )

                with open(filename, 'w', encoding='utf-8') as file:
                    file.write(updated_content)

                if pattern.search(file_content):
                    add_import_to_file(filename, "import static org.junit.jupiter.api.Assertions.assertThrows;")

            except Exception as e:
                print(f"Error replacing test statements in {filename}: {e}")


def add_import_to_file(filename, import_to_add):
    with open(filename, 'r', encoding='utf-8') as file:
        lines = file.readlines()

    is_added = False

    with open(filename, 'w', encoding='utf-8') as file:
        for line in lines:
            if "class" in line and is_added is False:
                file.write(f"{import_to_add}\n\n")
                file.write(line)
                is_added = True
            else:
                file.write(line)


def update_junit(pom_content, filenames):
    """
    updates junit dependencies to junit-jupiter in the POM, then searches the project for common junit code and replaces it with the equivalent junit-jupiter code
    """
    PomUpgrade.delete_dependency(pom_content, 'junit', 'junit')
    PomUpgrade.update_or_add_dependency(pom_content, "org.junit.jupiter", "junit-jupiter", "5.9.3", "junit-jupiter.version")
    PomUpgrade.update_or_add_dependency(pom_content, "org.junit.jupiter", "junit-jupiter-api", "5.9.3","junit-jupiter.version")
    PomUpgrade.update_or_add_dependency(pom_content, "org.junit.jupiter", "junit-jupiter-engine", "5.9.3","junit-jupiter.version")
    _junit_remove_annotated_test_exception_statements(filenames)
    _update_junit_imports(filenames, "import static org.junit.Assert.assertThat;","import static org.hamcrest.MatcherAssert.assertThat;")
    _update_junit_imports(filenames, "import static org.mockito.Mockito.verifyZeroInteractions;","import static org.mockito.Mockito.verifyNoInteractions;")
    _replace_junit_statements(filenames, "verifyZeroInteractions", "verifyNoInteractions")
    _update_junit_imports(filenames, "import org.junit.Test;", "import org.junit.jupiter.api.Test;")
    _update_junit_imports(filenames, "import static junit.framework.TestCase.assertTrue;","import static org.junit.jupiter.api.Assertions.assertTrue;")
    _update_junit_imports(filenames, "import org.junit.runner.RunWith;","import org.junit.jupiter.api.extension.ExtendWith;")
    _replace_junit_statements(filenames, "@RunWith(SpringRunner.class)", "@ExtendWith(SpringExtension.class)")
    _replace_junit_statements(filenames, "@RunWith(SpringJUnit4ClassRunner.class)","@ExtendWith(SpringExtension.class)")
    _replace_junit_statements(filenames, "@RunWith(MockitoJUnitRunner.class)", "@ExtendWith(MockitoExtension.class)")
    _update_junit_imports(filenames, "import org.springframework.test.context.junit4.SpringRunner;","import org.springframework.test.context.junit.jupiter.SpringExtension;")
    _update_junit_imports(filenames, "import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;","import org.springframework.test.context.junit.jupiter.SpringExtension;")
    _update_junit_imports(filenames, "import static org.junit.Assert","import static org.junit.jupiter.api.Assertions.*;")
    _update_junit_imports(filenames, "import org.junit.*;", "import org.junit.jupiter.api.*;")
    _update_junit_imports(filenames, "import org.mockito.junit.MockitoJUnitRunner;","import org.mockito.junit.jupiter.MockitoExtension;")
    _update_junit_imports(filenames, "import org.junit.Before;", "import org.junit.jupiter.api.BeforeEach;")
    _update_junit_imports(filenames, "import org.junit.After;", "import org.junit.jupiter.api.AfterEach;")
    _replace_junit_statements(filenames, "@AfterClass", "@AfterAll")
    _replace_junit_statements(filenames, "@After\n", "@AfterEach\n")
    _update_junit_imports(filenames, "import org.junit.Assert;", "import org.junit.jupiter.api.Assertions;")
    _update_junit_imports(filenames, "import org.junit.Assert.", "import org.junit.jupiter.api.Assertions.*;")
    _update_junit_imports(filenames, "import org.junit.BeforeClass;", "import org.junit.jupiter.api.BeforeAll;")
    _replace_junit_statements(filenames, "@BeforeClass", "@BeforeAll")
    _update_junit_imports(filenames, "import org.junit.AfterClass;", "import org.junit.jupiter.api.AfterAll;")
    _update_junit_imports(filenames, "import org.junit.Ignore;", "import org.junit.jupiter.api.Disabled;")
    _replace_junit_statements(filenames, " Assert.assert", " Assertions.assert")
    _replace_junit_statements(filenames, "@Before\n", "@BeforeEach\n")
    _replace_junit_statements(filenames, "@Ignore", "@Disabled")
